#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
using namespace cv;

int main(){
    Mat img = imread("./img/ruido.jpg",IMREAD_COLOR);
    Mat bilateralImage, gaussianImg,medianImg;
    GaussianBlur(img,gaussianImg , Size(15,15), 0);
    medianBlur(img, medianImg, 15);
    bilateralFilter(img, bilateralImage, 15,95,45);
    
    imshow("bilateral",bilateralImage);
    imshow("median",medianImg);
    imshow("gaussian",gaussianImg);
    imshow("Window",img);
    
    waitKey(0);
    return 0;
}
