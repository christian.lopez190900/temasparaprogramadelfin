import os

def files_content(option,nm):
    content = [None,None,None]
    content[0]=""
    content[1]="cmake_minimum_required(VERSION 2.8)\nproject( " + nm + " )\nfind_package( OpenCV REQUIRED )\ninclude_directories( ${OpenCV_INCLUDE_DIRS} )\nadd_executable(" + nm + " main.cpp )\ntarget_link_libraries( " + nm + " ${OpenCV_LIBS} )"
    content[2]="cp CMakeLists.txt ./Compile\ncp main.cpp ./Compile\ncp Imgs ./Compile\ncd Compile\ncmake .\nmake"
    return content[option]
def files_name(option):
    names = ["main.cpp", "CMakeLists.txt" , "compile.sh"]
    return names[option]
def crear_carpeta(nombre_carpeta):
    try:
        os.mkdir(nombre_carpeta)
        print("Carpeta creada correctamente.")
    except FileExistsError:
        print("La carpeta ya existe.")

def crear_carpeta_con_subcarpetas(nombre_carpeta, subcarpetas):
    crear_carpeta(nombre_carpeta)
    os.chdir(nombre_carpeta)  # Cambiar al directorio de la nueva carpeta
    for subcarpeta in subcarpetas:
        crear_carpeta(subcarpeta)
    os.chdir("..")  # Volver al directorio anterior

def crear_archivo(nombre_archivo, contenido=""):
    with open(nombre_archivo, "w") as archivo:
        archivo.write(contenido)
    print(f"Archivo '{nombre_archivo}' creado correctamente.")

def agregar_archivos_carpeta_principal(nombre_carpeta, archivos):
    os.chdir(nombre_carpeta)  # Cambiar al directorio de la carpeta principal
    for archivo, contenido in archivos.items():
        crear_archivo(archivo, contenido)
    os.chdir("..")  # Volver al directorio anterior

def main():
    nombre = input("Ingresa el nombre del Proyecto")
    nms = "Compile,Imgs"
    subcarpetas = nms.split(",")

    archivos = {}
    num_archivos = 3 #numero de archivos a crear
    for i in range(num_archivos):
        nombre_archivo = files_name(i)
        contenido_archivo = files_content(i,nombre)
        archivos[nombre_archivo] = contenido_archivo

    crear_carpeta_con_subcarpetas(nombre, subcarpetas)
    agregar_archivos_carpeta_principal(nombre, archivos)

if __name__ == "__main__":
    main()

