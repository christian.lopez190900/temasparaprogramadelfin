#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>

using namespace cv;

int main(){
    
    Mat img = imread("./Imgs/erosion.png");

    Mat eroMat, DilMat;
    Mat elementKernel;
    //erosion
    erode(img,eroMat,elementKernel,Point(-1,-1),1);

    //imshow("eroded",eroMat);

    //Dilatation
    dilate(img,DilMat,elementKernel,Point(-1,-1),1);
    
    imshow("Dilatation",DilMat);



    imshow("input",img);
    waitKey(0);

    return 0;
}
